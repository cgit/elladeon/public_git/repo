.TH pk12util 1 "August 2010" "USER COMMANDS"
.SH NAME 
 pk12util \- Export and import keys and certificate to or from a PKCS #12 file and the NSS database
.SH Synopsis 
.B pk12util 
-i p12File [-h tokenname] [-v] [common-options]  
         -l p12File [-h tokenname] [-r] [common-options]  
         -o p12File -n certname [-c keyCipher] [-C certCipher] [-m|--key_len keyLen] [-n|--cert_key_len certKeyLen] [common-options]  
 
 common-options are: 
 [-d dir] [-P dbprefix] [-k slotPasswordFile|-K slotPassword] [-w p12filePasswordFile|-W p12filePassword]  
       
.SH Description 

.PP
The PKCS #12 utility, 
.B pk12util
, enables sharing certificates among any server that supports PKCS#12. The tool can import certificates and keys from PKCS#12 files into security databases, export certificates, and list certificates and keys.
.SH Options and Arguments 

.PP
.B Options

.TP 
.B -i p12file

.IP
Import keys and certificates from a PKCS#12 file into a security database.

.TP 
.B -l p12file

.IP
List the keys and certificates in PKCS#12 file.

.TP 
.B -o p12file

.IP
Export keys and certificates from the security database to a PKCS#12 file.

.PP
.B Arguments

.TP 
.B -n certname

.IP
Specify the nickname of the cert and private key to export.

.TP 
.B -d dir

.IP
Specify the database directory into which to import to or export from certificates and keys. 
          If not specified the directory defaults to $HOME/.netscape (when $HOME exists in the environment), 
          or to ./.netscape (when $HOME does not exist in the environment.

.TP 
.B -P prefix

.IP
Specify the prefix used on the cert8.db and key3.db files 
          (for example, my_cert8.db and my_key3.db). This option is provided as a special case. 
          Changing the names of the certificate and key databases is not recommended.

.TP 
.B -h tokenname

.IP
Specify the name of the token to import into or export from.

.TP 
.B -v 

.IP
Enable debug logging when importing.

.TP 
.B -k slotPasswordFile

.IP
Specify the text file containing the slot's password.

.TP 
.B -K slotPassword

.IP
Specify the slot's password.

.TP 
.B -w p12filePasswordFile

.IP
Specify the text file containing the pkcs #12 file password.

.TP 
.B -W p12filePassword

.IP
Specify the pkcs #12 file password.

.TP 
.B -c keyCipher

.IP
Specify the key encryption algorithm.

.TP 
.B -C certCipher

.IP
Specify the key cert (overall package) encryption algorithm.

.TP 
.B -m | --key-len  keyLength

.IP
Specify the desired length of the symmetric key to be used to encrypt the private key.

.TP 
.B -n | --cert-key-len  certKeyLength

.IP
Specify the desired length of the symmetric key to be used to encrypt the certificates and other meta-data.

.TP 
.B -r

.IP
Dumps all of the data in raw (binary) form. This must be saved as a DER file. The default is to return information in a pretty-print ASCII format, which displays the information about the certificates and public keys in the p12 file.

.SH Return Codes 

.PP
* 0 - No error
.PP
* 1 - User Cancelled
.PP
* 2 - Usage error
.PP
* 6 - NLS init error
.PP
* 8 - Certificate DB open error
.PP
* 9 - Key DB open error
.PP
* 10 - File initialization error
.PP
* 11 - Unicode conversion error
.PP
* 12 - Temporary file creation error
.PP
* 13 - PKCS11 get slot error
.PP
* 14 - PKCS12 decoder start error
.PP
* 15 - error read from import file
.PP
* 16 - pkcs12 decode error
.PP
* 17 - pkcs12 decoder verify error
.PP
* 18 - pkcs12 decoder validate bags error
.PP
* 19 - pkcs12 decoder import bags error
.PP
* 20 - key db conversion version 3 to version 2 error
.PP
* 21 - cert db conversion version 7 to version 5 error
.PP
* 22 - cert and key dbs patch error
.PP
* 23 - get default cert db error
.PP
* 24 - find cert by nickname error
.PP
* 25 - create export context error
.PP
* 26 - PKCS12 add password itegrity error
.PP
* 27 - cert and key Safes creation error
.PP
* 28 - PKCS12 add cert and key error
.PP
* 29 - PKCS12 encode error
.SH Examples 

.PP
.B Importing Keys and Certificates

.PP
The most basic usage of 
.B pk12util
for importing a certificate or key is the PKCS#12 input file (
.B -i
) and some way to specify the security database being accessed (either 
.B -d
for a directory or 
.B -h
for a token).
    
.nf
pk12util -i p12File [-h tokenname] [-v] [-d dir] [-P dbprefix] [-k slotPasswordFile|-K slotPassword] [-w p12filePasswordFile|-W p12filePassword]
.fi

.PP
For example:
.nf
# pk12util -i /tmp/cert-files/users.p12 -d /etc/pki/nssdb/

Enter a password which will be used to encrypt your keys.
The password should be at least 8 characters long,
and should contain at least one non-alphabetic character.

Enter new password: 
Re-enter password: 
Enter password for PKCS12 file: 
pk12util: PKCS12 IMPORT SUCCESSFUL
.fi

.PP
.B Exporting Keys and Certificates

.PP
Using the 
.B pk12util
command to export certificates and keys requires both the name of the certificate to extract from the database (
.B -n
) and the PKCS#12-formatted output file to write to. There are optional parameters that can be used to encrypt the file to protect the certificate material.
    
.nf
pk12util -o p12File -n certname [-c keyCipher] [-C certCipher] [-m|--key_len keyLen] [-n|--cert_key_len certKeyLen] [-d dir] [-P dbprefix] [-k slotPasswordFile|-K slotPassword] [-w p12filePasswordFile|-W p12filePassword]
.fi

.PP
For example:
.nf
# pk12util -o certs.p12 -n Server-Cert -d /etc/pki/nssdb/
Enter password for PKCS12 file: 
Re-enter password: 
.fi

.PP
.B Listing Keys and Certificates

.PP
The information in a 
.B .p12
file are not human-readable. The certificates and keys in the file can be printed (listed) in a human-readable pretty-print format that shows information for every certificate and any public keys in the 
.B .p12
file. Alternatively, the 
.B -r
is used to print the certificates and then export them into separate DER binary files, with one certificate in each file. This allows the certificates to be fed to another application, like OpenSSL.
    
.nf
pk12util -l p12File [-h tokenname] [-r] [-d dir] [-P dbprefix] [-k slotPasswordFile|-K slotPassword] [-w p12filePasswordFile|-W p12filePassword]
.fi

.PP
For example, this prints the default ASCII output:
.nf
# pk12util -l certs.p12

Enter password for PKCS12 file: 
Key(shrouded):
    Friendly Name: Thawte Freemail Member's Thawte Consulting (Pty) Ltd. ID

    Encryption algorithm: PKCS #12 V2 PBE With SHA-1 And 3KEY Triple DES-CBC
        Parameters:
            Salt:
                45:2e:6a:a0:03:4d:7b:a1:63:3c:15:ea:67:37:62:1f
            Iteration Count: 1 (0x1)
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 13 (0xd)
        Signature Algorithm: PKCS #1 SHA-1 With RSA Encryption
        Issuer: "E=personal-freemail@thawte.com,CN=Thawte Personal Freemail C
            A,OU=Certification Services Division,O=Thawte Consulting,L=Cape T
            own,ST=Western Cape,C=ZA"
....
.fi

.PP
For example, this uses the 
.B -r
argument to output the DER information. Each certificate is also written to a sequentially-number file, beginning with 
.B file0001.der
and continuing through 
.B file000N.der
, incrementing the number for every certificate:
.nf
# pk12util -l test.p12 -r
Enter password for PKCS12 file: 
Key(shrouded):
    Friendly Name: Thawte Freemail Member's Thawte Consulting (Pty) Ltd. ID

    Encryption algorithm: PKCS #12 V2 PBE With SHA-1 And 3KEY Triple DES-CBC
        Parameters:
            Salt:
                45:2e:6a:a0:03:4d:7b:a1:63:3c:15:ea:67:37:62:1f
            Iteration Count: 1 (0x1)
Certificate    Friendly Name: Thawte Personal Freemail Issuing CA - Thawte Consulting

Certificate    Friendly Name: Thawte Freemail Member's Thawte Consulting (Pty) Ltd. ID
.fi

.SH Password Encryption 

.PP
PKCS#12 provides for not only the protection of the private keys but also the certificate and meta-data associated with the keys. Password-based encryption is used to protect private keys on export to a PKCS#12 file and, optionally, the entire package. If no algorithm is specified, the tool defaults to using 
.B PKCS12 V2 PBE with SHA1 and 3KEY Triple DES-cbc
for private key encryption. 
.B PKCS12 V2 PBE with SHA1 and 40 Bit RC4
is the default for the overall package encryption when not in FIPS mode. When in FIPS mode, there is no package encryption.
.PP
The private key is always protected with strong encryption by default.
.PP
Several types of ciphers are supported.
.TP 
.B Symmetric CBC ciphers for PKCS#5 V2

.IP
* DES_CBC
.IP
* RC2-CBC
.IP
* RC5-CBCPad
.IP
* DES-EDE3-CBC (the default for key encryption)
.IP
* AES-128-CBC
.IP
* AES-192-CBC
.IP
* AES-256-CBC
.IP
* CAMELLIA-128-CBC
.IP
* CAMELLIA-192-CBC
.IP
* CAMELLIA-256-CBC

.TP 
.B PKCS#12 PBE ciphers

.IP
* PKCS #12 PBE with Sha1 and 128 Bit RC4
.IP
* PKCS #12 PBE with Sha1 and 40 Bit RC4
.IP
* PKCS #12 PBE with Sha1 and Triple DES CBC
.IP
* PKCS #12 PBE with Sha1 and 128 Bit RC2 CBC
.IP
* PKCS #12 PBE with Sha1 and 40 Bit RC2 CBC
.IP
* PKCS12 V2 PBE with SHA1 and 128 Bit RC4
.IP
* PKCS12 V2 PBE with SHA1 and 40 Bit RC4 (the default for non-FIPS mode)
.IP
* PKCS12 V2 PBE with SHA1 and 3KEY Triple DES-cbc
.IP
* PKCS12 V2 PBE with SHA1 and 2KEY Triple DES-cbc
.IP
* PKCS12 V2 PBE with SHA1 and 128 Bit RC2 CBC
.IP
* PKCS12 V2 PBE with SHA1 and 40 Bit RC2 CBC

.TP 
.B PKCS#5 PBE ciphers

.IP
* PKCS #5 Password Based Encryption with MD2 and DES CBC
.IP
* PKCS #5 Password Based Encryption with MD5 and DES CBC
.IP
* PKCS #5 Password Based Encryption with SHA1 and DES CBC

.PP
With PKCS#12, the crypto provider may be the soft token module or an external hardware module. If the cryptographic module does not support the requested algorithm, then the next best fit will be selected (usually the default). If no suitable replacement for the desired algorithm can be found, the tool returns the error 
.I no security module can perform the requested operation
.
.SH See Also 

.PP
certutil (1)
.PP
modutil (1)
.SH Additional Resources 

.PP
NSS is maintained in conjunction with PKI and security-related projects through Mozilla dn Fedora. The most closely-related project is Dogtag PKI, with a project wiki at http://pki.fedoraproject.org/wiki/. 
.PP
For information specifically about NSS, the NSS project wiki is located at http://www.mozilla.org/projects/security/pki/nss/. The NSS site relates directly to NSS code changes and releases.
.PP
Mailing lists: pki-devel@redhat.com and pki-users@redhat.com
.PP
IRC: Freenode at #dogtag-pki
.SH Authors 

.PP
The NSS tools were written and maintained by developers with Netscape and now with Red Hat.
.PP
Authors: Elio Maldonado <emaldona@redhat.com>, Deon Lackey <dlackey@redhat.com>.
	
.SH Copyright 

.PP
(c) 2010, Red Hat, Inc. Licensed under the GNU Public License version 2.
