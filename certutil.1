.TH certutil 1 "14 August, 2008" "USER COMMANDS"
.SH NAME 
 certutil \- Manage keys and certificate in the the NSS database.
.SH Synopsis 
.B certutil 
options [ arguments ]
.SH Description 

.PP
The Certificate Database Tool, 
.B certutil
, is a command-line utility that can create and modify 
.B cert8.db
and 
.B key3.db
database files. It can also list, generate, modify, or delete certificates within the 
.B cert8.db
file and create or change the password, generate new public and private key pairs, display the contents of the key database, or delete key pairs within the 
.B key3.db
file.
	
.PP
The key and certificate management process generally begins with creating keys in the key database, then generating and managing certificates in the certificate database. This document discusses certificate and key database management. For information security module database management, see the 
.B modutil
manpages.
	
.SH Options and Arguments 

.PP
Running 
.B certutil
always requires one (and only one) option to specify the type of certificate operation. Each option may take arguments, anywhere from none to multiple arguments. Run the command option and 
.B -H
to see the arguments available for each command option.

	
.PP
.B Options

.PP
Options specify an action and are uppercase. 
.TP 
.B -A 

.IP
Add an existing certificate to a certificate database. The certificate database should already exist; if one is not present, this option will initialize one by default. 

.TP 
.B -B

.IP
Run a series of commands from the specified batch file. This requires the 
.B -i
argument.

.TP 
.B -C 

.IP
Create a new binary certificate file from a binary certificate request file. Use the 
.B -i
argument to specify the certificate request file. If this argument is not used, 
.B certutil
prompts for a filename. 

.TP 
.B -D 

.IP
Delete a certificate from the certificate database.

.TP 
.B -E 

.IP
Add an email certificate to the certificate database.

.TP 
.B -F

.IP
Delete a private key from a key database. Specify the key to delete with the -n argument. Specify the database from which to delete the key with the 

.B -d
argument. Use the 
.B -k
argument to specify explicitly whether to delete a DSA, RSA, or ECC key. If you don't use the 
.B -k
argument, 
the option looks for an RSA key matching the specified nickname. 

.IP
When you delete keys, be sure to also remove any certificates associated with those keys from the certificate database, by using -D. Some smart cards (for example, the Litronic card) do not let you remove a public key you have generated. In such a case, only the private key is deleted from the key pair. You can display the public key with the command certutil -K -h tokenname. 

.TP 
.B -G 

.IP
Generate a new public and private key pair within a key database. The key database should already exist; if one is not present, this option will initialize one by default. Some smart cards (for example, the Litronic card) can store only one key pair. If you create a new key pair for such a card, the previous pair is overwritten.

.TP 
.B -H 

.IP
Display a list of the options and arguments used by the Certificate Database Tool.

.TP 
.B -K 

.IP
List the key ID of keys in the key database. A key ID is the modulus of the RSA key or the publicValue of the DSA key. IDs are displayed in hexadecimal ("0x" is not shown).

.TP 
.B -L 

.IP
List all the certificates, or display information about a named certificate, in a certificate database.
Use the -h tokenname argument to specify the certificate database on a particular hardware or software token.

.TP 
.B -M 

.IP
Modify a certificate's trust attributes using the values of the -t argument.

.TP 
.B -N

.IP
Create new certificate and key databases.

.TP 
.B -O 

.IP
Print the certificate chain.

.TP 
.B -R

.IP
Create a certificate request file  that can be submitted to a Certificate Authority (CA) for processing into a finished certificate. Output defaults to standard out unless you use -o output-file argument.

Use the -a argument to specify ASCII output.

.TP 
.B -S 

.IP
Create an individual certificate and add it to a certificate database.

.TP 
.B -T 

.IP
Reset the key database or token.

.TP 
.B -U 

.IP
List all available modules or print a single named module.

.TP 
.B -V 

.IP
Check the validity of a certificate and its attributes.

.TP 
.B -W 

.IP
Change the password to a key database.

.TP 
.B --merge

.IP
Merge a source database into the target database.

.TP 
.B --upgrade-merge

.IP
Upgrade an old database and merge it into a new database.

.PP
.B Arguments

.PP
Option arguments modify an action and are lowercase.
.TP 
.B -a

.IP
Use ASCII format or allow the use of ASCII format for input or output. This formatting follows RFC 1113. 
For certificate requests, ASCII output defaults to standard output unless redirected.

.TP 
.B -b validity-time

.IP
Specify a time at which a certificate is required to be valid. Use when checking certificate validity with the 
.B -V
option. The format of the 
.I validity-time
argument is 
.I YYMMDDHHMMSS[+HHMM|-HHMM|Z]
, which allows offsets to be set relative to the validity end time. Specifying seconds (
.I SS
) is optional. When specifying an explicit time, use a Z at the end of the term, 
.I YYMMDDHHMMSSZ
, to close it. When specifying an offset time, use 
.I YYMMDDHHMMSS+HHMM
or 
.I YYMMDDHHMMSS-HHMM
for adding or subtracting time, respectively.

.IP
If this option is not used, the validity check defaults to the current system time.

.TP 
.B -c issuer

.IP
Identify the certificate of the CA from which a new certificate will derive its authenticity. 
 Use the exact nickname or alias of the CA certificate, or use the CA's email address. Bracket the issuer string 
 with quotation marks if it contains spaces. 

.TP 
.B -d directory

.IP
Specify the database directory containing the certificate and key database files. 

 The cert8.db and key3.db database files must reside in the same directory. 

.TP 
.B -e 

.IP
Check a certificate's signature during the process of validating a certificate.

.TP 
.B -f password-file

.IP
Specify a file that will automatically supply the password to include in a certificate 
 or to access a certificate database. This is a plain-text file containing one password. Be sure to prevent 
 unauthorized access to this file.

.TP 
.B -g keysize

.IP
Set a key size to use when generating new public and private key pairs. The minimum is 512 bits and the maximum is 8192 bits. The default is 1024 bits. Any size between the minimum and maximum is allowed.

.TP 
.B -h tokenname

.IP
Specify the name of a token to use or act on. Unless specified otherwise the default token is an internal slot (specifically, internal slot 2). This slot can also be explicitly named with the string "internal". An internal slots is a virtual slot maintained in software, rather than a hardware device. Internal slot 2 is used by key and certificate services. Internal slot 1 is used by cryptographic services.

.TP 
.B -i input_file

.IP
Pass an input file to the command. Depending on the command option, an input file can be a specific certificate, a certificate request file, or a batch file of commands.

.TP 
.B -k rsa|dsa|ec|all

.IP
Specify the type of a key. The valid options are RSA, DSA, ECC, or all. The default value is rsa. Specifying the type of key can avoid mistakes caused by duplicate nicknames.

.TP 
.B -k key-type-or-id

.IP
Specify the type or specific ID of a key. Giving a key type generates a new key pair; giving the ID of an existing key reuses that key pair (which is required to renew certificates).

.TP 
.B -l 

.IP
Display detailed information when validating a certificate with the -V option.

.TP 
.B -m serial-number

.IP
Assign a unique serial number to a certificate being created. This operation should be performed by a CA. The default serial number is 0 (zero). Serial numbers are limited to integers. 

.TP 
.B -n nickname

.IP
Specify the nickname of a certificate or key to list, create, add to a database, modify, or validate. Bracket the nickname string with quotation marks if it contains spaces.

.TP 
.B -o output-file

.IP
Specify the output file name for new certificates or binary certificate requests. Bracket the output-file string with quotation marks if it contains spaces. If this argument is not used the output destination defaults to standard output.

.TP 
.B -P dbPrefix

.IP
Specify the prefix used on the cert8.db and key3.db files 
(for example, my_cert8.db and my_key3.db). This option is provided as a special case. 
Changing the names of the certificate and key databases is not recommended.

.TP 
.B -p phone

.IP
Specify a contact telephone number to include in new certificates or certificate requests. Bracket this string with quotation marks if it contains spaces.

.TP 
.B -q pqgfile

.IP
Read an alternate PQG value from the specified file when generating DSA key pairs. If this argument is not used, 
.B certutil
generates its own PQG value. PQG files are created with a separate DSA utility.

.TP 
.B -q curve-name

.IP
Set the elliptic curve name to use when generating ECC key pairs. A complete list of ECC curves is given in the help (
.B -H
).

.TP 
.B -r 

.IP
Display a certificate's binary DER encoding when listing information about that certificate with the -L option.

.TP 
.B -s subject

.IP
Identify a particular certificate owner for new certificates or certificate requests. Bracket this string with quotation marks if it contains spaces. The subject identification format follows RFC #1485.

.TP 
.B -t trustargs

.IP
Specify the trust attributes to modify in an existing certificate or to apply to a certificate when creating it or adding it to a database. There are three available trust categories for each certificate, expressed in the order 
.I SSL, email, object signing
for each trust setting. In each category position, use none, any, or all
of the attribute codes: 
	
.IP
* 
.B p
- Valid peer
	
.IP
* 
.B P
- Trusted peer (implies p)
	
.IP
* 
.B c
- Valid CA
	
.IP
* 
.B T
- Trusted CA to issue client certificates (implies c)
	
.IP
* 
.B C
- Trusted CA to issue server certificates (SSL only) (implies c)
	
.IP
* 
.B u
- Certificate can be used for authentication or signing
	
.IP
* 
.B w
- Send warning (use with other attributes to include a warning when the certificate is used in that context)
	
.IP
The attribute codes for the categories are separated by commas, and the entire set of attributes enclosed by quotation marks. For example:
	
.IP
.B -t "TCu,Cu,Tuw"

.IP
Use the -L option to see a list of the current certificates and trust attributes in a certificate database. 

.TP 
.B -u certusage

.IP
Specify a usage context to apply when validating a certificate with the -V option.
.IP
The contexts are the following:
.IP
* 
.B C
(as an SSL client)
.IP
* 
.B V
(as an SSL server)
.IP
* 
.B S
(as an email signer)
.IP
* 
.B R
(as an email recipient)
.IP
* 
.B O
(as an OCSP status responder)
.IP
* 
.B J
(as an object signer)

.TP 
.B -v valid-months

.IP
Set the number of months a new certificate will be valid. The validity period begins at the current system time unless an offset is added or subtracted with the 
.B -w
option. If this argument is not used, the default validity 
period is three months. When this argument is used, the default three-month period 
is automatically added to any value given in the valid-month argument. For example, 
using this option to set a value of 3 would cause 3 to be added to the three-month 
default, creating a validity period of six months. You can use negative values to 
reduce the default period. For example, setting a value of -2 would subtract 2 from 
the default and create a validity period of one month.

.TP 
.B -w offset-months

.IP
Set an offset from the current system time, in months, 
 for the beginning of a certificate's validity period. Use when creating 
 the certificate or adding it to a database. Express the offset in integers, 
 using a minus sign (-) to indicate a negative offset. If this argument is 
 not used, the validity period begins at the current system time. The length 
 of the validity period is set with the -v argument. 

.TP 
.B -X 

.IP
Force the key and certificate database to open in read-write mode. This is used with the 
.B -U
and 
.B -L
command options.

.TP 
.B -x 

.IP
Use 
.B certutil
to generate the signature for a certificate being created or added to a database, rather than obtaining a signature from a separate CA.

.TP 
.B -y exp

.IP
Set an alternate exponent value to use in generating a new RSA public key for the database, instead of the default value of 65537. The available alternate values are 3 and 17.

.TP 
.B -z noise-file

.IP
Read a seed value from the specified file to generate a new private and public key pair. This argument makes it possible to use hardware-generated seed values or manually create a value from the keyboard. The minimum file size is 20 bytes.

.TP 
.B -0 SSO_password

.IP
Set a site security officer password on a token.

.TP 
.B -1 | --keyUsage keyword,keyword

.IP
Set a Netscape Certificate Type Extension in the certificate. There are several available keywords:
.IP
* digital signature
	
.IP
* nonRepudiation
	
.IP
* keyEncipherment
	
.IP
* dataEncipherment
	
.IP
* keyAgreement
	
.IP
* certSigning
	
.IP
* crlSigning
	
.IP
* critical
	

.TP 
.B -2 

.IP
Add a basic constraint extension to a certificate that is being created or added to a database. This extension supports the certificate chain verification process. 
.B certutil
prompts for the certificate constraint extension to select.
.IP
X.509 certificate extensions are described in RFC 5280.

.TP 
.B -3 

.IP
Add an authority key ID extension to a certificate that is being created or added to a database. This extension supports the identification of a particular certificate, from among multiple certificates associated with one subject name, as the correct issuer of a certificate. The Certificate Database Tool will prompt you to select the authority key ID extension.
.IP
X.509 certificate extensions are described in RFC 5280.

.TP 
.B -4 

.IP
Add a CRL distribution point extension to a certificate that is being created or added to a database. This extension identifies the URL of a certificate's associated certificate revocation list (CRL). 
.B certutil
prompts for the URL.
.IP
X.509 certificate extensions are described in RFC 5280.

.TP 
.B -5 | --nsCertType keyword,keyword

.IP
Add a Netscape certificate type extension to a certificate that is being created or added to the database. There are several available keywords:
.IP
* sslClient
	
.IP
* sslServer
	
.IP
* smime
	
.IP
* objectSigning
	
.IP
* sslCA
	
.IP
* smimeCA
	
.IP
* objectSigningCA
	
.IP
* critical
	
.IP
X.509 certificate extensions are described in RFC 5280.

.TP 
.B -6 | --extKeyUsage keyword,keyword

.IP
Add an extended key usage extension to a certificate that is being created or added to the database. Several keywords are available:
.IP
* serverAuth
	
.IP
* clientAuth
	
.IP
* codeSigning
	
.IP
* emailProtection
	
.IP
* timeStamp
	
.IP
* ocspResponder
	
.IP
* stepUp
	
.IP
* critical
	
.IP
X.509 certificate extensions are described in RFC 5280.

.TP 
.B -7 emailAddrs

.IP
Add a comma-separated list of email addresses to the subject alternative name extension of a certificate or certificate request that is being created or added to the database. Subject alternative name extensions are described in Section 4.2.1.7 of RFC 3280.

.TP 
.B -8 dns-names

.IP
Add a comma-separated list of DNS names to the subject alternative name extension of a certificate or certificate request that is being created or added to the database. Subject alternative name extensions are described in Section 4.2.1.7 of RFC 3280.

.TP 
.B --extAIA

.IP
Add the Authority Information Access extension to the certificate. X.509 certificate extensions are described in RFC 5280.

.TP 
.B --extSIA

.IP
Add the Subject Information Access extension to the certificate. X.509 certificate extensions are described in RFC 5280.

.TP 
.B --extCP

.IP
Add the Certificate Policies extension to the certificate. X.509 certificate extensions are described in RFC 5280.

.TP 
.B --extPM

.IP
Add the Policy Mappings extension to the certificate. X.509 certificate extensions are described in RFC 5280.

.TP 
.B --extPC

.IP
Add the Policy Constraints extension to the certificate. X.509 certificate extensions are described in RFC 5280.

.TP 
.B --extIA

.IP
Add the Inhibit Any Policy Access extension to the certificate. X.509 certificate extensions are described in RFC 5280.

.TP 
.B --extSKID

.IP
Add the Subject Key ID extension to the certificate. X.509 certificate extensions are described in RFC 5280.

.TP 
.B --source-dir certdir

.IP
Identify the certificate database directory to upgrade.

.TP 
.B --source-prefix certdir

.IP
Give the prefix of the certificate and key databases to upgrade.

.TP 
.B --upgrade-id uniqueID

.IP
Give the unique ID of the database to upgrade.

.TP 
.B --upgrade-token-name name

.IP
Set the name of the token to use while it is being upgraded.

.TP 
.B -@ pwfile

.IP
Give the name of a password file to use for the database being upgraded.

.SH Usage and Examples 

.PP
Most of the command options in the examples listed here have more arguments available. The arguments included in these examples are the most common ones or are used to illustrate a specific scenario. Use the 
.B -H
option to show the complete list of arguments for each command option.
	
.PP
.B Creating New Security Databases

.PP
Certificates, keys, and security modules related to managing certificates are stored in three related databases:
	
.PP
* cert8.db
	
.PP
* key3.db
	
.PP
* secmod.db
	
.PP
These databases must be created before certificates or keys can be generated.
	
.nf
certutil -N -d <directory>
.fi

.PP
.B Creating a Certificate Request

.PP
A certificate request contains most or all of the information that is used to generate the final certificate. This request is submitted separately to a certificate authority and is then approved by some mechanism (automatically or by human review). Once the request is approved, then the certificate is generated.
	
.nf
$ certutil -R -k key-type-or-id [-q pqgfile|curve-name] -g key-size -s subject [-h tokenname] -d certdir [-p phone] [-o output-file] [-a]
.fi

.PP
The 
.B -R
command options requires four arguments:
	
.PP
* 
.B -k
to specify either the key type to generate or, when renewing a certificate, the existing key pair to use
	
.PP
* 
.B -g
to set the keysize of the key to generate
	
.PP
* 
.B -s
to set the subject name of the certificate
	
.PP
* 
.B -d
to give the security database directory
	
.PP
The new certificate request can be output in ASCII format (
.B -a
) or can be written to a specified file (
.B -o
).
	
.PP
For example:
	
.nf
$ certutil -R -k ec -q nistb409 -g 512 -s "CN=John Smith,O=Example Corp,L=Mountain View,ST=California,C=US" -d . -p 650-555-0123 -a -o cert.

Generating key.  This may take a few moments...


Certificate request generated by Netscape 
Phone: 650-555-0123
Common Name: John Smith
Email: (not ed)
Organization: Example Corp
State: California
Country: US

-----BEGIN NEW CERTIFICATE REQUEST-----
MIIBIDCBywIBADBmMQswCQYDVQQGEwJVUzETMBEGA1UECBMKQ2FsaWZvcm5pYTEW
MBQGA1UEBxMNTW91bnRhaW4gVmlldzEVMBMGA1UEChMMRXhhbXBsZSBDb3JwMRMw
EQYDVQQDEwpKb2huIFNtaXRoMFwwDQYJKoZIhvcNAQEBBQADSwAwSAJBAMVUpDOZ
KmHnOx7reP8Cc0Lk+fFWEuYIDX9W5K/BioQOKvEjXyQZhit9aThzBVMoSf1Y1S8J
CzdUbCg1+IbnXaECAwEAAaAAMA0GCSqGSIb3DQEBBQUAA0EAryqZvpYrUtQ486Ny
qmtyQNjIi1F8c1Z+TL4uFYlMg8z6LG/J/u1E5t1QqB5e9Q4+BhRbrQjRR1JZx3tB
1hP9Gg==
-----END NEW CERTIFICATE REQUEST-----
.fi

.PP
.B Creating a Certificate

.PP
A valid certificate must be issued by a trusted CA. This can be done by specifying a CA certificate (
.B -c
) that is stored in the certificate database. If a CA key pair is not available, you can create a self-signed certificate using the 
.B -x
argument with the 
.B -S
command option.
	
.nf
$ certutil -S -k rsa|dsa|ec -n certname -s subject [-c issuer |-x] -t trustargs -d directory [-m serial-number] [-v valid-months] [-w offset-months] [-p phone] [-1] [-2] [-3] [-4] [-5 keyword] [-6 keyword] [-7 emailAddress] [-8 dns-names] [--extAIA] [--extSIA] [--extCP] [--extPM] [--extPC] [--extIA] [--extSKID]
.fi

.PP
The series of numbers and 
.B --ext*
options set certificate extensions that can be added to the certificate when it is generated by the CA.
	
.PP
For example, this creates a self-signed certificate:
	
.nf
$ certutil -S -s "CN=Example CA" -n my-ca-cert -x -t "C,C,C" -1 -2 -5 -m 3650
.fi

.PP
From there, new certificates can reference the self-signed certificate:
	
.nf
$ certutil -S -s "CN=My Server Cert" -n my-server-cert -c "my-ca-cert" -t "u,u,u" -1 -5 -6 -8 -m 730
.fi

.PP
.B Generating a Certificate from a Certificate Request

.PP
When a certificate request is created, a certificate can be generated by using the request and then referencing a certificate authority signing certificate (the 
.I issuer
specified in the 
.B -c
argument). The issuing certificate must be in the 
.B cert8.db
database in the specified directory.
	
.nf
certutil -C -c issuer -i cert-request-file -o output-file [-m serial-number] [-v valid-months] [-w offset-months] -d certdir [-1] [-2] [-3] [-4] [-5 keyword] [-6 keyword] [-7 emailAddress] [-8 dns-names]
.fi

.PP
For example:
	
.nf
$ certutil -C -c "my-ca-cert" -i /home/certs/cert.req -o cert.cer -m 010 -v 12 -w 1 -d . -1 nonRepudiation,dataEncipherment -5 sslClient -6 clientAuth -7 jsmith@example.com
.fi

.PP
.B Generating Key Pairs

.PP
Key pairs are generated automatically with a certificate request or certificate, but they can also be generated independently using the 
.B -G
command option. 
	
.nf
certutil -G -d directory | -h tokenname -k key-type -g key-size [-y exponent-value] -q pqgfile|curve-name
.fi

.PP
For example:
	
.nf
$ certutil -G -h lunasa -k ec -g 256 -q sect193r2
.fi

.PP
.B Listing Certificates

.PP
The 
.B -L
command option lists all of the certificates listed in the 
.B cert8.db
database. The path to the directory (
.B -d
) is required.
	
.nf
$ certutil -L -d .

Certificate Nickname                                         Trust Attributes
                                                             SSL,S/MIME,JAR/XPI

CA Administrator of Instance pki-ca1's Example Domain ID     u,u,u
TPS Administrator's Example Domain ID                        u,u,u
Google Internet Authority                                    ,,   
Certificate Authority - Example Domain                       CT,C,C
.fi

.PP
Using additional arguments with 
.B -L
can return and print the information for a single, specific certificate. For example, the 
.B -n
argument passes the certificate name, while the 
.B -a
argument prints the certificate in ASCII format:
	
.nf
$ certutil -L -d . -a -n "Certificate Authority - Example Domain"

-----BEGIN CERTIFICATE-----
MIIDmTCCAoGgAwIBAgIBATANBgkqhkiG9w0BAQUFADA5MRcwFQYDVQQKEw5FeGFt
cGxlIERvbWFpbjEeMBwGA1UEAxMVQ2VydGlmaWNhdGUgQXV0aG9yaXR5MB4XDTEw
MDQyOTIxNTY1OFoXDTEyMDQxODIxNTY1OFowOTEXMBUGA1UEChMORXhhbXBsZSBE
b21haW4xHjAcBgNVBAMTFUNlcnRpZmljYXRlIEF1dGhvcml0eTCCASIwDQYJKoZI
hvcNAQEBBQADggEPADCCAQoCggEBAO/bqUli2KwqXFKmMMG93KN1SANzNTXA/Vlf
Tmrih3hQgjvR1ktIY9aG6cB7DSKWmtHp/+p4PUCMqL4ZrSGt901qxkePyZ2dYmM2
RnelK+SEUIPiUtoZaDhNdiYsE/yuDE8vQWj0vHCVL0w72qFUcSQ/WZT7FCrnUIUI
udeWnoPSUn70gLhcj/lvxl7K9BHyD4Sq5CzktwYtFWLiiwV+ZY/Fl6JgbGaQyQB2
bP4iRMfloGqsxGuB1evWVDF1haGpFDSPgMnEPSLg3/3dXn+HDJbZ29EU8/xKzQEb
3V0AHKbu80zGllLEt2Zx/WDIrgJEN9yMfgKFpcmL+BvIRsmh0VsCAwEAAaOBqzCB
qDAfBgNVHSMEGDAWgBQATgxHQyRUfKIZtdp55bZlFr+tFzAPBgNVHRMBAf8EBTAD
AQH/MA4GA1UdDwEB/wQEAwIBxjAdBgNVHQ4EFgQUAE4MR0MkVHyiGbXaeeW2ZRa/
rRcwRQYIKwYBBQUHAQEEOTA3MDUGCCsGAQUFBzABhilodHRwOi8vbG9jYWxob3N0
LmxvY2FsZG9tYWluOjkxODAvY2Evb2NzcDANBgkqhkiG9w0BAQUFAAOCAQEAi8Gk
L3XO43u7/TDOeEsWPmq+jZsDZ3GZ85Ajt3KROLWeKVZZZa2E2Hnsvf2uXbk5amKe
lRxdSeRH9g85pv4KY7Z8xZ71NrI3+K3uwmnqkc6t0hhYb1mw/gx8OAAoluQx3biX
JBDxjI73Cf7XUopplHBjjiwyGIJUO8BEZJ5L+TF4P38MJz1snLtzZpEAX5bl0U76
bfu/tZFWBbE8YAWYtkCtMcalBPj6jn2WD3M01kGozW4mmbvsj1cRB9HnsGsqyHCu
U0ujlL1H/RWcjn607+CTeKH9jLMUqCIqPJNOa+kq/6F7NhNRRiuzASIbZc30BZ5a
nI7q5n1USM3eWQlVXw==
-----END CERTIFICATE-----
.fi

.PP
.B Listing Keys

.PP
Keys are the original material used to encrypt certificate data. The keys generated for certificates are stored separately, in the 
.B key3.db
database. 
	
.PP
To list all keys in the database, use the 
.B -K
command option and the (required) 
.B -d
argument to give the path to the directory.
	
.nf
$ certutil -K -d .
certutil: Checking token "NSS Certificate DB" in slot "NSS User Private Key and Certificate Services                  "
< 0> rsa      455a6673bde9375c2887ec8bf8016b3f9f35861d   Thawte Freemail Member's Thawte Consulting (Pty) Ltd. ID
< 1> rsa      40defeeb522ade11090eacebaaf1196a172127df   Example Domain Administrator Cert
< 2> rsa      1d0b06f44f6c03842f7d4f4a1dc78b3bcd1b85a5   John Smith user cert
.fi

.PP
There are ways to narrow the keys listed in the search results:
	
.PP
* To return a specific key, use the 
.B -n

.I name
argument with the name of the key.
	
.PP
* If there are multiple security devices loaded, then the 
.B -h

.I tokenname
argument can search a specific token or all tokens.
	
.PP
* If there are multiple key types available, then the 
.B -k

.I key-type
argument can search a specific type of key, like RSA, DSA, or ECC. 
	
.PP
.B Listing Security Modules

.PP
The devices that can be used to store certificates -- both internal databases and external devices like smart cards -- are recognized and used by loading security modules. The 
.B -U
command option lists all of the security modules listed in the 
.B secmod.db
database. The path to the directory (
.B -d
) is required.
	
.nf
$ certutil -U -d .

    slot: NSS User Private Key and Certificate Services                  
   token: NSS Certificate DB

    slot: NSS Internal Cryptographic Services                            
   token: NSS Generic Crypto Services
.fi

.PP
.B Adding Certificates to the Database

.PP
Existing certificates or certificate requests can be added manually to the 
.B cert8.db
database, even if they were generated elsewhere. This uses the 
.B -A
command option.
	
.nf
certutil -A -n certname -t trustargs -d certdir [-a] [-i input-file]
.fi

.PP
For example:
	
.nf
$ certutil -A -n "CN=My SSL Certificate" -t "u,u,u" -d . -i /home/example-certs/cert.cer
.fi

.PP
A related command option, 
.B -E
, is used specifically to add email certificates to the certificate database. The 
.B -E
command has the same arguments as the 
.B -A
command. The trust arguments for certificates have the format 
.I SSL,S/MIME,Code-signing
, so the middle trust settings relate most to email certificates (though the others can be set). For example:
	
.nf
$ certutil -E -n "CN=John Smith Email Cert" -t ",Pu," -d . -i /home/example-certs/email.cer
.fi

.PP
.B Deleting Certificates to the Database

.PP
Certificates can be deleted from a database using the 
.B -D
option. The only required options are to give the security database directory and to identify the certificate nickname.
	
.nf
certutil -D -d directory -n "nickname"
.fi

.PP
For example:
	
.nf
$ certutil -D -d . -n "my-ssl-cert"
.fi

.PP
.B Validating Certificates

.PP
A certificate contains an expiration date in itself, and expired certificates are easily rejected. However, certificates can also be revoked before they hit their expiration date. Checking whether a certificate has been revoked requires validating the certificate. Validation can also be used to ensure that the certificate is only used for the purposes it was initially issued for. Validation is carried out by the 
.B -V
command option.
	
.nf
certutil -V -n certificate-name [-b time] [-e] [-u cert-usage] -d directory
.fi

.PP
For example, to validate an email certificate:
	
.nf
$ certutil -V -n "John Smith's Email Cert" -e -u S,R -d .
.fi

.PP
.B Modifying Certificate Trust Settings

.PP
The trust settings (which relate to the operations that a certificate is allowed to be used for) can be changed after a certificate is created or added to the database. This is especially useful for CA certificates, but it can be performed for any type of certificate.
	
.nf
certutil -M -n certificate-name -t trust-args -d directory
.fi

.PP
For example:
	
.nf
$ certutil -M -n "My CA Certificate" -d . -t "CTu,CTu,CTu"
.fi

.PP
.B Printing the Certificate Chain

.PP
Certificates can be issued in 
.I chains
because every certificate authority itself has a certificate; when a CA issues a certificate, it essentially stamps that certificate with its own fingerprint. The 
.B -O
prints the full chain of a certificate, going from the initial CA (the root CA) through ever intermediary CA to the actual certificate. For example, for an email certificate with two CAs in the chain:
	
.nf
$ certutil -d . -O -n "jsmith@example.com"
"Builtin Object Token:Thawte Personal Freemail CA" [E=personal-freemail@thawte.com,CN=Thawte Personal Freemail CA,OU=Certification Services Division,O=Thawte Consulting,L=Cape Town,ST=Western Cape,C=ZA]

  "Thawte Personal Freemail Issuing CA - Thawte Consulting" [CN=Thawte Personal Freemail Issuing CA,O=Thawte Consulting (Pty) Ltd.,C=ZA]

    "(null)" [E=jsmith@example.com,CN=Thawte Freemail Member]
.fi

.PP
.B Resetting a Token

.PP
The device which stores certificates -- both external hardware devices and internal software databases -- can be blanked and reused. This operation is performed on the device which stores the data, not directly on the security databases, so the location must be referenced through the token name (
.B -h
) as well as any directory path. If there is no external token used, the default value is internal.
	
.nf
certutil -T -d directory -h token-name -0 security-officer-password
.fi

.PP
Many networks have dedicated personnel who handle changes to security tokens (the security officer). This person must supply the password to access the specified token. For example:
	
.nf
$ certutil -T -d . -h nethsm -0 secret
.fi

.PP
.B Upgrading or Merging the Security Databases

.PP
Some networks or applications may be using older versions of the certificate database, like a 
.B cert7.db
database, or there may be multiple certificate databases in use. Databases can be upgraded to a new version using the 
.B --upgrade-merge
command option or merged with other databases using the 
.B ---merge
command.
	
.PP
The 
.B --upgrade-merge
command must give information about the original database and then use the standard arguments (like 
.B -d
) to give the information about the new databases. The command also requires information that the tool uses for the process to upgrade and write over the original database.
	
.nf
certutil --upgrade-merge -d directory [-P dbprefix] --source-dir directory --source-prefix dbprefix --upgrade-id id --upgrade-token-name name [-@ password-file]
.fi

.PP
For example:
	
.nf
$ certutil --upgrade-merge -d . --source-dir /opt/my-app/alias/ --source-prefix serverapp- --upgrade-id 1 --upgrade-token-name internal
.fi

.PP
The 
.B --merge
command only requires information about the location of the original database; since it doesn't change the format of the database, it can write over information without performing interim step.
	
.nf
certutil --merge -d directory [-P dbprefix] --source-dir directory --source-prefix dbprefix [-@ password-file]
.fi

.PP
For example:
	
.nf
$ certutil --merge -d . --source-dir /opt/my-app/alias/ --source-prefix serverapp-
.fi

.PP
.B Running certutil Commands from a Batch File

.PP
A series of commands can be run sequentially from a text file with the 
.B -B
command option. The only argument for this specifies the input file.
	
.nf
$ certutil -B -i /path/to/batch-file
.fi

.SH See Also 

.PP
.B certutil
has arguments or operations that use features defined in several IETF RFCs.
.PP
* http://tools.ietf.org/html/rfc5280
	
.PP
* http://tools.ietf.org/html/rfc1113
	
.PP
* http://tools.ietf.org/html/rfc1485
	
.SH Additional Resources 

.PP
NSS is maintained in conjunction with PKI and security-related projects through Mozilla dn Fedora. The most closely-related project is Dogtag PKI, with a project wiki at http://pki.fedoraproject.org/wiki/. 
.PP
For information specifically about NSS, the NSS project wiki is located at http://www.mozilla.org/projects/security/pki/nss/. The NSS site relates directly to NSS code changes and releases.
.PP
Mailing lists: pki-devel@redhat.com and pki-users@redhat.com
.PP
IRC: Freenode at #dogtag-pki
.SH Authors 

.PP
The NSS tools were written and maintained by developers with Netscape and now with Red Hat.
.PP
Authors: Elio Maldonado <emaldona@redhat.com>, Deon Lackey <dlackey@redhat.com>.
	
.SH Copyright 

.PP
(c) 2010, Red Hat, Inc. Licensed under the GNU Public License version 2.
